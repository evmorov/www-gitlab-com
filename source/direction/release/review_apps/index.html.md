---
layout: markdown_page
title: "Category Vision - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build a review process right into your software development workflow
by automatically provisioning test environments for your code, integrated right
into your merge requests.

This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=review%20apps&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1299) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next up is making review apps more obvious and easy to get started with [gitlab-ce#58596](https://gitlab.com/gitlab-org/gitlab-ce/issues/58596)
which will help more users get up and running successfully. These are a great
differentiator for GitLab and it's important we put our best foot forward with
this feature.

After this we plan to provide a mechanism to make it easier to clean up stale
environments [gitlab-ce#38718](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718).

## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [Truly dynamic environment URLs](https://gitlab.com/gitlab-org/gitlab-ce/issues/27424)
- [Mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718)
- [Optional expiration time for environments](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822)
- [First-class review apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/35849)

## Competitive Landscape

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [gitlab-ce#27893](https://gitlab.com/gitlab-org/gitlab-ce/issues/27893),
which does the heavy lifting of getting them working for you.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, www-gitlab-com project has over
1,500 running stale environments at the time of writing this, with no clear easy way
to clean them up. There are two main items that look to address this challenge:

- [gitlab-ce#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822) builds in an expiration date for review apps, beyond which they will automatically be terminated.
- [gitlab-ce#38718](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718) (also the #2 customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.

The first issue does not help instances that already have the problem, but we will
go after it first as it stops the issue from becoming worse. After that, we will
look at the need for the second (which may be able to be built on top of the first.)

## Top Customer Issue(s)

The top customer issue impacting users of Review Apps is [gitlab-ce#27424](https://gitlab.com/gitlab-org/gitlab-ce/issues/27424),
which enables truly dynamic URLs, giving additional flexibility for teams to name their review apps.

## Top Internal Customer Issue(s)

Adding review apps for GitLab Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621))
makes the process for validating websites much easier to use. Some projects have
implemented this individually (for example, www-gitlab-com), but we could provide
a solid out of the box solution for this.

Additionally, more efficient allocation of VMs for review apps (through, for example,
more efficient use of nginx) via issues like [gitlab-ce#59519](https://gitlab.com/gitlab-org/gitlab-ce/issues/59519)
has been requested internally to help us better manage resources.

## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via
[gitlab-ce#40683](https://gitlab.com/gitlab-org/gitlab-ce/issues/40683) - adding
support to Android/iOS emulators via the Review App will enable a whole new kind
of development workflow in our product, and make Review Apps even more valuable.
