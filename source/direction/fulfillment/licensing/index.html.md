---
layout: markdown_page
title: "Category Vision - Licensing"
---

- TOC
{:toc}

## 💌 Licensing

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

* [Overall Vision](/direction/fulfillment)
* [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=licensing) 
* [Maturity: Minimal](/product/categories/maturity)
* [Documentation](https://docs.gitlab.com/ee/user/admin_area/license.html#uploading-your-license)
* [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=licensing)

The Licensing category covers GitLab's [License application](https://gitlab.com/gitlab-org/license-gitlab-com), and aims to fulfill users licensing needs, as well as enabling GitLab Billing and Sales administrators to distribute GitLab license keys.

Please reach out to  Luca Williams, Product Manager for Fulfillment ([E-Mail](luca@gitlab.com)/
[Twitter](https://twitter.com/tipyn2903)) if you'd like to provide feedback or ask
questions about what's coming.

## 🔭 Vision  

Handling license keys should require minimal if not zero effort for both users and GitLab team members. Our goal is to significantly reduce or remove any possibility of an interruption in service for our users due to license key issues, and we want to reduce or remove the need for manual technical intervention of GitLab team members.

In addition, licensing should be user-friendly and we should avoid using language that assumes that users are abusing license keys in the event their license key has expired, has been applied to the wrong instance, or does not match the instance's purchased seat count. We should assume positive intent with our users and provide empathic, straightforward help and assistance to ensuring their license key is correct and up to date.

Design and user experience is an overall priority for Fulfillment. Please visit this [meta epic](https://gitlab.com/groups/gitlab-org/-/epics/1176) to see ongoing design and UX improvements to the Fulfillment group's categories.

## 🎭 Target audience  
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

### Sidney (Systems Administrator) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)

* 🙂 **Minimal** - Sidney can receive a license key in some form and is able to apply it to their GitLab instance.
* 😊 **Viable** - Sidney is able to manage their GitLab license keys in a centralised location and easily apply them to their organisation's GitLab instances. 
* 😁 **Complete** - Sidney is able to perform all the things in the viable solution, and additionally is able to view license key metadata (e.g. which instance a license key is currently applied to, expiration dates, and min/max seats).
* 😍 **Lovable** - Sidney doesn't need to do anything to manage their GitLab license keys. License keys are synchronized automatically from GitLab's license key server and are always up to date.

#### If Sidney's instances are not able to connect to external servers:
* 😁 **Complete** - Sidney is able to perform all the things in the viable solution, and additionally is able to view key metadata (e.g. which instance a key is currently applied to, expiration dates, and min/max seats) which was relevant at the time of key application. 
* 😍 **Lovable** - Sidney is able to copy/paste their instances license keys into their account on GitLab's license server, which then automatically true-ups their license and generates a payment request. A license key will be generated upon payment and Sidney will be notified that they can now apply their new license.  

### Skye (GitLab Billing/Sales Administrator) - [Persona Description TBD]

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

### Kennedy (Sales Representative) - [Persona Description TBD]

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's next & why 
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

TBD.

<!--  ## 🏅 Competitive landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ## Analyst landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ## Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

* [Category issues listed by popularity](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=licensing&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

## 🦊 Top internal customer issues/epics  
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

* [Findings from supporting upgrades/renewal queue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1519)

<!--  ## Top Vision Item(s)
What's the most important thing to move your vision forward?-->
