---
layout: markdown_page
title: "GitLab Security Training"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}

----
## What is this? <a name="what"></a>
The GitLab Security Training is GitLab's security awareness presentation for new hires and annual training requirements. The training is originally part of the onboarding process, and needs to be completed by every new hire. We are trying to make it fun, engaging and not time-consuming.

## What's the reasoning behind this effort and who's driving it?<a name="why"></a>

The training is being actively developed by [GitLab Security](https://about.gitlab.com/handbook/engineering/security/)'s [Security Operations](https://about.gitlab.com/handbook/engineering/security/#security-operations) team. The goal of the training is to:

1. Make all GitLabbers aware of the GitLab Security team, and familiarise them with our efforts, team structure, and people.
1. Make all GitLabbers aware of the importance of their role in securing GitLab on a daily basis, and to empower them to make the right decisions with security best-practices.
1. Familiarise all new GitLabbers with security-related situations that they might encounter during their tenure with the company.
1. Help all GitLabbers internalise and reinforce the idea that paging the Security On-Call is an encouraged practice.

## Delivery
The New Hire Security Orientation is delivered through a pre-recorded presentation that is presented by a member of the [Security Operations](https://about.gitlab.com/handbook/engineering/security/#security-operations) team. The following materials are made available for your consumption:
* [The video](https://www.youtube.com/watch?v=kNc9f-LH6pU) - the actual training.
* [The slides](https://docs.google.com/presentation/d/1Yts7RixM9Eb7wt9x9TqS2lAHjmHzKcEI_-j_yZaztlc/) - the slides used in the training video.
* [Security Best Practices in short](https://about.gitlab.com/handbook/security/#best-practices) - a list of key take-aways that you can consult at any time.

## Feedback
You are strongly encouraged to engage the team behind the New Hire Security Orientation and provide feedback, or ask any questions related to the contant of the training. You can do that through:
1. A non-mandatory monthly Q&A session that is held every last Friday of the month. If you don't have an invite on your calendar, please let Security know about it in #security.
1. A quarterly-reviewed GitLab issue - [2019 Q1](https://gitlab.com/gitlab-com/gl-security/operations/issues/182).
1. Email by sending an email to security-training@gitlab.com.

