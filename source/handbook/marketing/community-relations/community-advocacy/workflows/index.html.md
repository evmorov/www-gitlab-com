---
layout: markdown_page
title: "Community advocacy workflows"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Community response workflows

- [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews)
- [Education and Open Source](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource)
- [Twitter](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)
- [Website comments](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments)
- [E-mail](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail)
- [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/reddit)
- [Stack Overflow](/handbook/marketing/community-relations/community-advocacy/workflows/stackoverflow)
- [Inactive workflows](/handbook/marketing/community-relations/community-advocacy/workflows/inactive)

## Other workflows

- [Involving experts](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts)
- [Knowledge base](/handbook/marketing/community-relations/community-advocacy/workflows/knowledge-base)
- [#movingtogitlab](/handbook/marketing/community-relations/community-advocacy/workflows/moving-to-gitlab)
- [Advocate for a Day](/handbook/marketing/community-relations/community-advocacy/workflows/advocate-for-a-day)
