---
layout: markdown_page
title: "Periscope Directory"
description: "GitLab Periscope Directory"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

Request Access to Periscope with an [Access Request Issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New+Access+Request)

## Periscope Resources
* [Periscope Training](https://drive.google.com/file/d/1FS5llpZlfvlFyYL-4kpP3YUgI98c_rKB/view?usp=sharing) (GitLab Internal)
* [Periscope Editor Training](https://drive.google.com/file/d/15tm_zomS2Ny6NdWiUNJlZ0_73THDiDww/view?usp=sharing) (GitLab Internal)
* **[Periscope Data Onboarding: Creating and Analyzing Charts/Dashboards](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be)**
* **[Getting Started With Periscope Data!](https://doc.periscopedata.com/article/getting-started)**
* [Periscope Guides](https://www.periscopedata.com/resources#guides)
* [Periscope Community](https://community.periscopedata.com)
* [Documentation](https://doc.periscopedata.com)

## Verified Periscope Dashboard

Some dashboards in Periscope will include a Verified Checkmark.
![Periscope Verified Checkmark](/handbook/business-ops/data-team/periscope-directory/periscope_verified_checkmark.jpg)

That means these analyses have been reviewed by the data team for query accuracy.
Dashboards without the verified checkmark are not necessarily inaccurate;
they just haven't been reviewed by the data team.


## Spaces

We have two Periscope [spaces](https://doc.periscopedata.com/article/spaces#article-title):
* GitLab
* GitLab Sensitive

They connect to the data warehouse with different users- `periscope` and `periscope_sensitive` respectively.

Most work is present in the GitLab space, though some _extremely sensitive analyses_ will be limited to GitLab sensitive. Examples of this may include analyses involving contractor and employee compensation and unanonymized interviewing data.

Spaces are organized with tags. Tags should map to function (Product, Marketing, Sales, etc) and subfunction (Create, Secure, Field Marketing, EMEA). Tags should loosely match [issue labels](handbook/business-ops/data-team/#issue-labeling) (no prioritization).
Tags are free. Make it as easy as possible for people to find the information they're looking for. At this time, tags cannot be deleted or renamed.

* [Example Dashboard](https://app.periscopedata.com/app/gitlab/403199/Example-Dashboard)

### GitLab Space

* [GitLab KPIs](https://app.periscopedata.com/app/gitlab/434327/)
* [GitLab Metrics](https://app.periscopedata.com/app/gitlab/409920/)
* [Distribution of Customers to ARR](https://app.periscopedata.com/app/gitlab/416459/)
   * [Customers by ARR Rank](https://app.periscopedata.com/app/gitlab/416590/)
* [GitLab.com Customer Retention](https://app.periscopedata.com/app/gitlab/412223/)
* [GitLab.com Financial KPIs](https://app.periscopedata.com/app/gitlab/444436/)
* [GitLab.com Sandbox](https://app.periscopedata.com/app/gitlab/406359/)
* [GitLab Downloads by Installation Type](https://app.periscopedata.com/app/gitlab/428908/)
* [Month over Month SMAU](https://app.periscopedata.com/app/gitlab/425984/)
* [New Business Sales Cycle - Trailing 12 Months](https://app.periscopedata.com/app/gitlab/408541/)
* [Retention](https://app.periscopedata.com/app/gitlab/403244/)
   * [ARR by Fiscal year Cohort](https://app.periscopedata.com/app/gitlab/422746/)
   * [Subscription Information and MRR](https://app.periscopedata.com/app/gitlab/422776/)
   * [Parent Account Retention by Size and Segment](https://app.periscopedata.com/app/gitlab/422779/)
* [Sales](https://app.periscopedata.com/app/gitlab/423607/)
   * [Won Opportunities Drill Down](https://app.periscopedata.com/app/gitlab/425606/)
   * [SAO Opportunity Drill Down](https://app.periscopedata.com/app/gitlab/425981/)
   * [SQO Opportunity Drill Down](https://app.periscopedata.com/app/gitlab/425982/)
* [Sales Forecast](https://app.periscopedata.com/app/gitlab/408411/)
* [Secure Metrics](https://app.periscopedata.com/app/gitlab/410654/)
* [Snowplow Summary Metrics](https://app.periscopedata.com/app/gitlab/417669/)
* [Trueups](https://app.periscopedata.com/app/gitlab/441679/)
* [Version Upgrade Rate](https://app.periscopedata.com/app/gitlab/406972/)
* [Zendesk Customer Support](https://app.periscopedata.com/app/gitlab/421422/)

#### Meta Metrics for the Data Team

* [Periscope Usage! 📈](https://app.periscopedata.com/app/gitlab/410320/)
* [Periscope Account Optimization 💪](https://app.periscopedata.com/app/gitlab/410321/)
* [Periscope Account Maintenance 🗑️](https://app.periscopedata.com/app/gitlab/410322/)
* [dbt Event Logging](https://app.periscopedata.com/app/gitlab/420622/)

### GitLab Sensitive Space

This space is not used at this time.

## Pushing Dashboards into Slack Automattically

Many folks will have some cadence on which they want to see dashboards; 
for example, Product wants an update on opportunities lost of product reasons every week.
Where it is best that this info is piped into Slack on a regular cadence, you can take advantage of Slack's native `/remind` to print the URL.
If it does not appear that the dashboard is autorefreshing, please ping a [Periscope admin](/handbook/business-ops/#tech-stack) to update the refresh schedule.

## User Roles

There are three user roles (Access Levels) in Periscope: admin, SQL, and View Only.

The current status of Periscope licenses can be found in [the analytics project](https://gitlab.com/gitlab-data/analytics/blob/master/analyze/periscope_users.yml).

<div class="panel panel-info">
**Updating Users for Periscope**
{: .panel-heading}
<div class="panel-body">

* Inject jquery into the console (from [StackOverflow](https://stackoverflow.com/questions/45042129/how-can-i-use-jquery-in-the-firefox-scratchpad)):

```javascript
let jq = document.createElement("script");
jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js";
jq.onload = function() {
  //your code here
};
document.body.appendChild(jq);
```

* On the Users page to get list of all users from the DOM, run this in the console.

```javascript
$('div.user-name').map(function(i, el) {
  return $(el).text()}
).toArray()
```

* To get all editors from the DOM, on the Groups page, after clicking on the Editors group, run this in the console:

```javascript
$('.name-text-with-globe').map(function(i, el) {
  return $(el).text()}
).toArray()
```

</div>
</div>

### Administrators

These users have the ability to provision new users, change permissions, and edit database connections. (Typical admin things)

Resource: [Onboarding Admins](https://www.youtube.com/watch?v=e-cZgf6zzlQ&feature=youtu.be)

### Editor access

The users have the ability to write SQL queries against the `analytics` schema of the `analytics` database that underlie charts and dashboards. They can also create or utilize SQL snippets to make this easier. **There are a limited number of SQL access licenses, so at this time we aim to limit teams to one per Director-led team. It will be up to the Director to decide on the best candidate on her/his team to have SQL access.**

### View only users

These users can consume all existing dashboards. They can change filters on dashboards. Finally, they can take advantage of the [Drill Down](https://doc.periscopedata.com/article/drilldowns) functionality to dig into dashboards.

### Notes for when provisioning users

Make an MR to the analytics repo updating the permissions file and link it in your provisioning message. This helps affirm who got access to what, when, and at what tier.

In the Periscope UI, navigate to the **Directory** (not the Settings. This is important since we have Spaces enabled) to add the new user using her/his first and last names and email. Then add the user to the "All Users" group  and their function group (e.g. Marketing, Product, etc.) by clicking the pencil icon on the right side of the page next to "Group". If it is an editor user, then add her/him to the "Editor" group.

Users will inherit the highest access from any group they are in. This is why all functions are by default View only.

Permissions for a group are maintained under the space "Settings" section. (This is very confusing.) To upgrade or downgrade a group, you need to do that under setting, not under the Directory.

## Dashboard Creation and Review Workflow

This section details the workflow of how to push a dashboard to "production" in Periscope. Currently, there is no functionality to have a MR-first workflow. This workflow is intended to ensure a high level of quality of all dashboards within the company. A dashboard is ready for production when the visuals, SQL, Python, and UX of the dashboard have been peer reviewed by a member of the data team and meet the standards detailed in the handbook.

1. Create a dashboard with `WIP:` as the name and add it to the `WIP` topic
1. Utilize the documentation of dbt and the warehouse to build your queries and charts
1. Once the dashboard is ready for review, create an MR [in this project](https://gitlab.com/gitlab-com/www-gitlab-com) adding the dashboard to this page and using the Periscope Dashboard Review template
1. Follow the instructions on the template
1. Assign the template to a member of the data team for review
1. Once all feedback has been given and applied, the data team member will update the text tile in the upper right corner detailing who created and reviewed the dashboard, when it was last updated, and cross-link relevant issues (See [Data Analysis Process](/handbook/business-ops/data-team/#-data-analysis-process) for more details)
1. The dashboard will be cross-linked to the above directory (with just the `Dashboard Name`, no WIP) and any original issue
1. The data team member reviewer will:
   * Rename the dashboard to remove the `WIP:` label
   * Remove the dashboard from the `WIP` topic
   * Add the Approval Badge to the dashboard
   * Merge the MR if they have permissions or assign it to someone with merge rights on the handbook
